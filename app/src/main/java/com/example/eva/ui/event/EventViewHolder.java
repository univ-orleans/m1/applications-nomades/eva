package com.example.eva.ui.event;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.EventActivity;
import com.example.eva.R;
import com.example.eva.database.event.Event;

public class EventViewHolder extends RecyclerView.ViewHolder {

	public final View view;
	public final TextView summaryView;
	public final TextView dateStartView;
	public final ImageView logoView;
	public final ImageView canceled_background;
	public final TextView canceled_text;
	public Event event;


	public EventViewHolder(@NonNull View itemView, final Environment activity) {
		super(itemView);
		this.view = itemView;
		this.summaryView = itemView.findViewById(R.id.event_summary);
		this.dateStartView = itemView.findViewById(R.id.event_date_start);
		this.logoView = itemView.findViewById(R.id.event_detail_icon_localisation);
		this.canceled_text = itemView.findViewById(R.id.event_canceled_text);
		this.canceled_background = itemView.findViewById(R.id.event_canceld_background);

		this.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, EventActivity.class);
				intent.putExtra(EventActivity.keyIdEvent,event.getId());
				activity.startActivityForResult(intent, 0);
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			}
		});

	}


	@NonNull
	@Override
	public String toString() {
		return super.toString() + " EventViewHolder{ summaryView=" + summaryView + ", dateStartView=" + dateStartView + "}";
	}
}
