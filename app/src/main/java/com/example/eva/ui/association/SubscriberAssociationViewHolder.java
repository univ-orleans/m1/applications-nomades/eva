package com.example.eva.ui.association;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.R;
import com.example.eva.database.association.Association;
import com.example.eva.service.api.google.calendar.DataSynchronisationWorker;

public class SubscriberAssociationViewHolder extends RecyclerView.ViewHolder {

    // -------------------- ATTRIBUTE
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    public final Switch subscribeView;
    public final ImageView logoView;
    public Association association;


    public SubscriberAssociationViewHolder(@NonNull View itemView, final Environment activity) {
        super(itemView);
        this.subscribeView = itemView.findViewById(R.id.association_subscribe);
        this.logoView = itemView.findViewById(R.id.association_icon);

        this.subscribeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getAssociationViewModel().update(association.setSubscribe(!association.getSubscribe()));
                if (association.getSubscribe()) {
                    DataSynchronisationWorker.sheduler(activity, association);
                }
            }
        });
    }


    @NonNull
    @Override
    public String toString() {
        return super.toString() + " SubscriberAssociationViewHolder{ nameView=" + this.subscribeView.getText() + "}";
    }
}
