package com.example.eva.ui.event;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.R;
import com.example.eva.database.association.Association;
import com.example.eva.database.event.Event;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventViewHolder> {

	private final List<Event> events;
	private final Environment activity;


	public EventRecyclerViewAdapter(List<Event> items, Environment activity) {
		this.events = items;
		this.activity = activity;
	}


	@NonNull
	@Override
	public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_event, parent, false);
		return new EventViewHolder(view, this.activity);
	}


	@SuppressLint({"SimpleDateFormat", "SetTextI18n"})
	@Override
	public void onBindViewHolder(final EventViewHolder holder, int position) {
		holder.event = this.events.get(position);
		holder.summaryView.setText(holder.event.getSummary());

		Calendar startOfTheDay = new GregorianCalendar();
		startOfTheDay.set(Calendar.HOUR_OF_DAY, 0);
		startOfTheDay.set(Calendar.MINUTE, 0);
		startOfTheDay.set(Calendar.SECOND, 0);
		startOfTheDay.set(Calendar.MILLISECOND, 0);
		Calendar startOfNextDay = (Calendar) startOfTheDay.clone();
		startOfNextDay.set(Calendar.DATE, startOfTheDay.get(Calendar.DATE) + 1);

		if (startOfTheDay.getTimeInMillis() <= holder.event.getDateStart().getTime() && holder.event.getDateStart().getTime() < startOfNextDay.getTimeInMillis()) {
			holder.dateStartView.setText("Aujourd'hui à " + new SimpleDateFormat("HH:mm").format(holder.event.getDateStart()));
		}
		else {
			holder.dateStartView.setText(new SimpleDateFormat("dd MMM yyyy à HH:mm").format(holder.event.getDateStart()));
		}

		if (holder.event.isCanceled()) {
			holder.canceled_text.setVisibility(TextView.VISIBLE);
			holder.canceled_background.setVisibility(ImageView.VISIBLE);
		}
		else {
			ConstraintLayout constraintLayout = (ConstraintLayout) holder.canceled_text.getParent();
			ConstraintSet constraintSet = new ConstraintSet();
			constraintSet.clone(constraintLayout);
			constraintSet.connect(R.id.event_canceled_text, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.END, 0);
			constraintSet.clear(R.id.event_canceled_text, ConstraintSet.END);
			constraintSet.applyTo(constraintLayout);
            holder.canceled_text.setVisibility(TextView.INVISIBLE);
            holder.canceled_background.setVisibility(ImageView.INVISIBLE);
		}

		Association association = this.activity.getAssociationViewModel().get(holder.event.getIdAssociation());
		if (association.getLogo() != null) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				holder.logoView.setForeground(null);
			}
			holder.logoView.setImageResource(association.getLogo());
		}
	}


	@Override
	public int getItemCount() {
		return this.events.size();
	}
}
