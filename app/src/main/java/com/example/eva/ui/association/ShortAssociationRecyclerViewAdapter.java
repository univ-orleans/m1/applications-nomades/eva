package com.example.eva.ui.association;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.R;
import com.example.eva.database.association.Association;
import com.example.eva.database.event.Event;

import java.util.List;

public class ShortAssociationRecyclerViewAdapter extends RecyclerView.Adapter<ShortAssociationViewHolder> {

    private final List<Association> associations;
    private final Environment activity;


    public ShortAssociationRecyclerViewAdapter(List<Association> items, Environment activity) {
        this.activity = activity;
        this.associations = items;
    }


    @NonNull
    @Override
    public ShortAssociationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_short_association, parent, false);
        return new ShortAssociationViewHolder(view, this.activity);
    }


    @Override
    public void onBindViewHolder(final ShortAssociationViewHolder holder, int position) {
        final Association association = this.associations.get(position);
        holder.association = association;
        holder.nameView.setText(association.getName());

        // mettre à jour le compteur d'événement suite à une synchronisation
        this.activity.getEventViewModel().getData().observe(this.activity, new Observer<List<Event>>() {
            @Override
            public void onChanged(List<Event> items) {
                int number = activity.getEventViewModel().numberConfirmed(association);
                if (number > 0) {
                    holder.counterView.setText(String.valueOf(number));
                }
                else {
                    holder.counterView.setVisibility(View.INVISIBLE);
                    holder.couterBackgroundView.setVisibility(View.INVISIBLE);
                }
            }
        });

        // afficher le logo de l'association si il existe
        if (association.getLogo() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { holder.logoView.setForeground(null); }
            holder.logoView.setImageResource(association.getLogo());
        }
    }


    @Override
    public int getItemCount() {
        return this.associations.size();
    }
}
