package com.example.eva;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.example.eva.database.association.AssociationViewModel;
import com.example.eva.database.event.Event;
import com.example.eva.database.event.EventViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

public class EventActivity extends Environment {


	public static final String keyIdEvent = "idEvent";



	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);
		Intent intent = getIntent();
		int id = intent.getIntExtra(EventActivity.keyIdEvent, -1);

		this.eventViewModel = new ViewModelProvider(this).get(EventViewModel.class);
		this.associationViewModel = new ViewModelProvider(this).get(AssociationViewModel.class);

		// mise en place de la barre d'action
		setContentView(R.layout.activity_event);
		setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
		Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

		Event event = eventViewModel.get(id);
		LinearLayout parent_contener = findViewById(R.id.event_detail_parent);

		// Titre de l'événement
		TextView title = findViewById(R.id.event_detail_title);
		title.setText(event.getSummary());

		// Date(s) de l'événement
		Calendar today = new GregorianCalendar();
		Calendar date_end = new GregorianCalendar();
		Calendar date_start = new GregorianCalendar();
		date_end.setTime(event.getDateEnd());
		date_start.setTime(event.getDateStart());

		StringBuilder builder_date_start = new StringBuilder();
		StringBuilder builder_date_end = new StringBuilder();
		SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH:mm");
		SimpleDateFormat dateFormatLongDate = new SimpleDateFormat("EEEE dd MMM yyyy");
		SimpleDateFormat dateFormatShortDate = new SimpleDateFormat("EEEE dd MMM");
		String separatorDot = " ● ";
		String separatorDash = " ─ ";

		if (isSameDay(date_start, date_end)) {
			// Si c'est aujourd'hui, format: Aujourd'hui ● 10:00 ─ 11:00
			if (isSameDay(date_start, today)) {
				builder_date_start.append("Aujourd'hui")
						.append(separatorDot)
						.append(dateFormatHour.format(date_start.getTime()))
						.append(separatorDash);
				builder_date_end.append(dateFormatHour.format(date_end.getTime()));
			}

			// Si c'est cette année, format: Jeudi 3 déc. ● 13:30 ─ 15:30
			else if (date_start.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
				builder_date_start.append(dateFormatShortDate.format(date_start.getTime())).append(separatorDot).append(dateFormatHour.format(date_start.getTime())).append(separatorDash);
				builder_date_end.append(dateFormatHour.format(date_end.getTime()));
			}

			// Si ce n'est pas cette année, format: Lundi 6 janv. 2021 ● 10:15 ─ 12:15
			else {
				builder_date_start.append(dateFormatLongDate.format(date_start.getTime())).append(separatorDot).append(dateFormatHour.format(date_start.getTime())).append(separatorDash);
				builder_date_end.append(dateFormatHour.format(date_end.getTime()));
			}
		} else {
			// Si c'est cette année, format: Jeudi 3 déc. ● 13:30 ─ vendredi 4 déc. ● 15:30
			if (date_start.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
				builder_date_start.append(dateFormatShortDate.format(date_start.getTime())).append(separatorDot).append(dateFormatHour.format(date_start.getTime())).append(separatorDash);
				builder_date_end.append(dateFormatShortDate.format(date_end.getTime())).append(separatorDot).append(dateFormatHour.format(date_end.getTime()));
			}

			// Si ce n'est pas cette année, format: Lundi 6 janv. 2021 ● 10:15 ─ Mardi 7 janv. 2021 ● 12:15
			else {
				builder_date_start.append(dateFormatLongDate.format(date_start.getTime())).append(separatorDot).append(dateFormatHour.format(date_start.getTime())).append(separatorDash);
				builder_date_end.append(dateFormatLongDate.format(date_end.getTime())).append(separatorDot).append(dateFormatHour.format(date_end.getTime()));
			}

			((LinearLayout) findViewById(R.id.event_detail_date_parent)).setOrientation(LinearLayout.VERTICAL);
		}
		((TextView) findViewById(R.id.event_detail_date_start)).setText(builder_date_start);
		((TextView) findViewById(R.id.event_detail_date_end)).setText(builder_date_end);

		// Status
		if (!event.isCanceled()) {
			parent_contener.removeView(findViewById(R.id.event_detail_status));
		}

		// Association
		TextView association = findViewById(R.id.event_detail_association);
		association.setText(associationViewModel.get(event.getIdAssociation()).getName());

		// Localisation
		if (event.getLocation() != null) {
			TextView localisation = findViewById(R.id.event_detail_localisation);
			localisation.setText(event.getLocation());
		}
		else {
			parent_contener.removeView(findViewById(R.id.event_detail_location_contener));
		}

		// Description
		if (event.getDescription() != null) {
			TextView description = findViewById(R.id.event_detail_description);
			description.setText(Html.fromHtml(event.getDescription()));
		}
		else {
			parent_contener.removeView(findViewById(R.id.event_detail_description_contener));
		}
	}


	@SuppressLint("SimpleDateFormat")
	private boolean isSameDay(Calendar date, Calendar other_date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(date.getTime()).equals(format.format(other_date.getTime()));
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}
}