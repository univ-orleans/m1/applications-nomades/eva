package com.example.eva;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.database.association.Association;
import com.example.eva.database.event.Event;
import com.example.eva.ui.event.EventRecyclerViewAdapter;

import java.util.List;
import java.util.Objects;

public class EventsAssociationActivity extends Environment {

	public static final String keyIdAssociation = "idAsso";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events_association);

		Intent intent = getIntent();
		int id = intent.getIntExtra(EventsAssociationActivity.keyIdAssociation,-1);
		final Association association = this.associationViewModel.get(id);

		// mise en place de la barre d'action
		setContentView(R.layout.activity_events_association);
		setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
		Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(association.getName());

		// mise à jour de l'interface utilisateur
		final Environment activity = this;
		this.eventViewModel.getData().observe(this, new Observer<List<Event>>() {
			@Override
			public void onChanged(@Nullable List<Event> items) {
				final RecyclerView recyclerView = findViewById(R.id.list);
				final ConstraintLayout information_no_event = findViewById(R.id.information_no_event);

				List<Event> events = eventViewModel.getListByAssociation(association);

				// si aucun événement n'est prévus
				if (events.size() == 0) {
					information_no_event.setVisibility(View.VISIBLE);
					recyclerView.setVisibility(View.INVISIBLE);
				}

				// sinon
				else {
					information_no_event.setVisibility(View.INVISIBLE);
					recyclerView.setVisibility(View.VISIBLE);
					recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
					recyclerView.setAdapter(new EventRecyclerViewAdapter(events, activity));
				}
			}
		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}
}