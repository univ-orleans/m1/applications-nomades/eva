package com.example.eva.database.association;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;

import java.util.List;


public class AssociationViewModel extends AndroidViewModel {

	// -------------------- ATTRIBUTE
	private final AssociationRepository repository;

	/**
	 * CONSTRUCTEUR
	 * @param application l'application
	 */
	public AssociationViewModel(Application application) {
		super(application);
		this.repository = new AssociationRepository(application);
	}


	/**
	 * Ajouter une nouvelle association
	 * @param association la nouvelle association
	 */
	public void insert(Association association) {
		this.repository.insert(association);
	}


	/**
	 * La mise à jour d'une association
	 * @param association l'association à mettre à jour
	 */
	public void update(Association association){
		this.repository.update(association);
	}

	/**
	 * Supprimer une association
	 * @param association l'association a supprimé
	 */
	public void remove(Association association) {
		this.repository.delete(association);
	}

	/**
	 * Donner une association en fonction de son identifiant
	 * @param id l'identifiant de l'association
	 * @return  l'association
	 */
	public Association get(int id){
		return this.repository.getAssociation(id);
	}

	/**
	 * Donner une association en fonction de son identifiant de google aganda
	 * @param idGoogleCalendar l'identifiant de l'association dans google aganda
	 * @return  l'association
	 */
	public Association get(String idGoogleCalendar){
		return this.repository.getAssociation(idGoogleCalendar);
	}


	/**
	 * La liste de toutes les associations
	 * @return La liste de toutes les associations
	 */
	public List<Association> getList() {
		return this.repository.getAllAssociationList();
	}

	/**
	 * La liste des associations ou l'utilisateur est abonné
	 * @return la liste des associations abonnées
	 */
	public List<Association> getSubscriberList() {
		return this.repository.getAllAssociationSubscribe();
	}
}
