package com.example.eva.database.association;


import android.app.Application;

import com.example.eva.database.EvaRoomDatabase;
import com.example.eva.database.MyDao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Repertoire de la liste de association
 */
public class AssociationRepository {

	// -------------------- ATTRIBUTE
	private final MyDao dao;


	/**
	 * CONSTRUCTOR
	 * @param application l'application
	 */
	public AssociationRepository(Application application) {
		this.dao = EvaRoomDatabase.getDatabase(application).myDao();
	}

	/**
	 * L'ajout d'une association dans BD
	 * @param association l'association ajoutée
	 */
	public void insert(final Association association) {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				dao.insertAssociation(association);
				return null;
			}
		});
	}

	/**
	 * La mise à jours d'une association dans la BD
	 * @param association l'association à mettre à jour
	 */
	public void update(final Association association) {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				dao.updateAssociation(association);
				return null;
			}
		});
	}


	/**
	 * Supprimer une association
	 * @param association l'association à supprimée
	 */
	public void delete(final Association association) {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				dao.deleteAssociation(association);
				return null;
			}
		});
	}


	/**
	 * Obtenir une association en fonction de son identifiant
	 * @param id l'identifiant de l'association
	 * @return l'association
	 */
	public Association getAssociation(final int id) {
		Association association = null;
		Future<Association> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<Association>() {
			@Override
			public Association call() {
				return dao.getAssociation(id);
			}
		});
		try { association = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return association;
	}

	/**
	 * Obtenir une association en fonction de son identifiant de google aganda
	 * @param idGoogleCalendar l'identifiant de l'association dans google aganda
	 * @return l'association
	 */
	public Association getAssociation(final String idGoogleCalendar) {
		Association association = null;
		Future<Association> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<Association>() {
			@Override
			public Association call() {
				return dao.getAssociation(idGoogleCalendar);
			}
		});
		try { association = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return association;
	}


	/**
	 * La liste de toutes les associations dans la BD
	 * @return la liste de toutes les associations
	 */
	public List<Association> getAllAssociationList() {
		List<Association> associations = new ArrayList<>();
		Future<List<Association>> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<List<Association>>() {
			@Override
			public List<Association> call() {
				return dao.getAllAssociationList();
			}
		});
		try { associations = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return associations;
	}

	/**
	 * La liste des associations ou l'utilisateur est abonné
	 * @return la liste des associations abonnées
	 */
	public List<Association> getAllAssociationSubscribe() {
		List<Association> associations = new ArrayList<>();
		Future<List<Association>> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<List<Association>>() {
			@Override
			public List<Association> call() {
				return dao.getAllAssociationSubscribe();
			}
		});
		try { associations = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return associations;
	}
}

