package com.example.eva.database.event;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.eva.database.EvaRoomDatabase;
import com.example.eva.database.MyDao;
import com.example.eva.database.association.Association;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


public class EventRepository {

	// -------------------- ATTRIBUTE
	private final MyDao dao;


	// -------------------- CONSTRUCTOR

	/**
	 * CONSTRUCTEUR
	 * @param application l'application
	 */
	public EventRepository(Application application) {
		this.dao = EvaRoomDatabase.getDatabase(application).myDao();
	}


	/**
	 * Ajouter un événement dans le BD
	 * @param event l'événement
	 */
	public void insert(final Event event) {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				dao.insertEvent(event);
				return null;
			}
		});
	}


	/**
	 * La mise à jour d'un événement dans le BD
	 * @param event l'événement
	 */
	public void update(final Event event) {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				dao.updateEvent(event);
				return null;
			}
		});
	}

	/**
	 * L'ajouter ou la mise à jour d'une liste événements
	 * @param events la liste d'événements
	 */
	public void insertUpdate(final List<Event> events) {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				Date update = new Date();
				for (Event event : events) {
					if (event != null) {
						Event oldEvent = dao.getEvent(event.setDateUpdated(update).getIdGoogleEvent());
						// si l'événement est déjà présent, ont le met à jour.
						if (oldEvent != null) {
							event.setId(oldEvent.getId());
							dao.updateEvent(event);
						}
						else {
							dao.insertEvent(event);
						}
					}
				}
				if (events.size() > 0) { dao.deleteOldEvents(update.getTime()); }
				return null;
			}
		});
	}


	/**
	 * Supprimer un événement
	 * @param event un événement
	 */
	public void delete(final Event event) {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				dao.deleteEvent(event);
				return null;
			}
		});
	}

	/**
	 * Supprimer tous les événements passés
	 */
	public void deleteOldEvents() {
		EvaRoomDatabase.EXECUTOR.submit(new Callable<Void>() {
			@Override
			public Void call() {
				dao.deleteOldEvents(new Date().getTime());
				return null;
			}
		});
	}


	/**
	 * Donner l'événement en fonctionde son identifiant
	 * @param id : l'identifiant de l'événement
	 * @return l'événement
	 */
	public Event getEvent(final int id) {
		Event event = null;
		Future<Event> future =  EvaRoomDatabase.EXECUTOR.submit(new Callable<Event>() {
			@Override
			public Event call() {
				return dao.getEvent(id);
			}
		});
		try { event = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return event;
	}

	/**
	 * Donner l'événement en fonction de son identifiant de google aganda
	 * @param idGoogleEvent l'identifiant de google aganda
	 * @return l'événement
	 */
	public Event getEvent(final String idGoogleEvent) {
		Event event = null;
		Future<Event> future =  EvaRoomDatabase.EXECUTOR.submit(new Callable<Event>() {
			@Override
			public Event call() {
				return dao.getEvent(idGoogleEvent);
			}
		});
		try { event = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return event;
	}


	/**
	 * Donner l'observateur sur la liste des événements
	 * @return l'observateur sur la liste des événements
	 */
	public LiveData<List<Event>> getData() {
		LiveData<List<Event>> data = new MutableLiveData<>();
		Future<LiveData<List<Event>>> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<LiveData<List<Event>>>() {
			@Override
			public LiveData<List<Event>> call() {
				return dao.getEventData();
			}
		});
		try { data = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return data;
	}


	/**
	 * La liste de tous les événements dans la BD
	 * @return le liste de tous les événements
	 */
	public List<Event> getAllEventList() {
		List<Event> events = new ArrayList<>();
		Future<List<Event>> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<List<Event>>() {
			@Override
			public List<Event> call() {
				return dao.getAllEventList();
			}
		});
		try { events = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return events;
	}

	/**
	 * La liste de tous les événements dans une période.
	 * C'est entre aujourd'hui et aujourd'hui + un nombre de jours
	 * @param daysNumber le nombre de jours
	 * @return la liste des événements
	 */
	public List<Event> getAllEventBeforeLimit(final int daysNumber) {
		List<Event> events = new ArrayList<>();
		Future<List<Event>> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<List<Event>>() {
			@Override
			public List<Event> call() {
				Calendar limit = new GregorianCalendar();
				limit.set(Calendar.DATE, limit.get(Calendar.DATE) + daysNumber);
				return dao.getAllEventBeforeLimit(new Date().getTime(), limit.getTimeInMillis());
			}
		});
		try { events = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return events;
	}

	/**
	 * La liste de tous les événements d'une association
	 * @param association une association
	 * @return  La liste de tous les événements d'une association
	 */
	public List<Event> getAllEventListByAssociation(final Association association) {
		List<Event> events = new ArrayList<>();
		Future<List<Event>> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<List<Event>>() {
			@Override
			public List<Event> call() {
				return dao.getAllEventByAssociation(new Date().getTime(), association.getId());
			}
		});
		try { events = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return events;
	}

	/**
	 * Le nombre d'événement confirmé pour une assocition
	 * @param association une association
	 * @return Le nombre d'événement confirmé pour une assocition
	 */
	public int numberEventConfirmedByAssociation(final Association association) {
		int number = 0;
		Future<Integer> future = EvaRoomDatabase.EXECUTOR.submit(new Callable<Integer>() {
			@Override
			public Integer call() {
				return dao.numberEventConfirmedByAssociation(new Date().getTime(), association.getId());
			}
		});
		try { number = future.get(); }
		catch (ExecutionException | InterruptedException e) { e.printStackTrace(); }
		finally { future.cancel(true); }
		return number;
	}
}

