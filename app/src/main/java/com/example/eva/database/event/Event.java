package com.example.eva.database.event;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.eva.database.association.Association;

import java.text.SimpleDateFormat;
import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;



@Entity(
	tableName = "event_table",
	foreignKeys = @ForeignKey (entity = Association.class, parentColumns = "id", childColumns = "idAssociation", onDelete = CASCADE),
	indices = @Index(name = "association_index", value ={"idAssociation"})
)
public class Event {

	// -------------------- ATTRIBUTE
	/**
	 * Idendifiant de l'événement.
	 */
	@PrimaryKey(autoGenerate = true)
	private int id;

	/**
	 * Une relation avec la classe Association: id
	 */
	@NonNull
	@ColumnInfo(name = "idAssociation")
	private Integer idAssociation;

	/**
	 * Idendifiant Google agenda de l'événement.
	 */
	@NonNull
	@ColumnInfo(name = "idGoogleEvent")
	private String idGoogleEvent;

	/**
	 * Nom de l'événement.
	 */
	@NonNull
	@ColumnInfo(name = "summary")
	private String summary;

	/**
	 * État de l'event.
	 */
	@NonNull
	@ColumnInfo(name = "canceled")
	private Boolean canceled;

	/**
	 * La date de début de l'événement.
	 */
	@NonNull
	@ColumnInfo(name = "dateStart")
	private Date dateStart;

	/**
	 * La date de fin de l'événement.
	 */
	@NonNull
	@ColumnInfo(name = "dateEnd")
	private Date dateEnd;

	/**
	 * La date de mise à jour de l'événement.
	 */
	@NonNull
	@ColumnInfo(name = "dateUpdated")
	private Date dateUpdated;

	/**
	 * Le lieu de l'événement.
	 */
	@Nullable
	@ColumnInfo(name = "location")
	private String location;

	/**
	 * La description de l'événement.
	 */
	@Nullable
	@ColumnInfo(name = "description")
	private String description;

	/**
	 * Constructeur
	 * @param idAssociation l'identifiant de l'association organisatrice
	 * @param idGoogleEvent l'identifiant de évènement dans google aganda
	 * @param summary le nom de l'évènement
	 * @param canceled indiquer si l'évènement a été supprimer
	 * @param dateStart la date de début de l'évènement
	 * @param dateEnd la date de fin de l'évènement
	 * @param dateUpdated la date de mise à jour de l'évènement
	 * @param location le lieu
	 * @param description la description de l'évènement
	 */
	public Event(@NonNull Integer idAssociation, @NonNull String idGoogleEvent, @NonNull String summary, @NonNull Boolean canceled, @NonNull Date dateStart, @NonNull Date dateEnd, @NonNull Date dateUpdated, @Nullable String location, @Nullable String description) {
		this.idAssociation = idAssociation;
		this.idGoogleEvent = idGoogleEvent;
		this.summary = summary;
		this.canceled = canceled;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.dateUpdated = dateUpdated;
		this.location = location;
		this.description = description;
	}


	// -------------------- GET
	public int getId() {
		return this.id;
	}

	@NonNull
	public Integer getIdAssociation() {
		return this.idAssociation;
	}

	@NonNull
	public String getIdGoogleEvent() {
		return this.idGoogleEvent;
	}

	@NonNull
	public String getSummary() {
		return this.summary;
	}

	@NonNull
	public Boolean isCanceled() {
		return this.canceled;
	}

	@NonNull
	public Date getDateStart() {
		return this.dateStart;
	}

	@NonNull
	public Date getDateEnd() {
		return this.dateEnd;
	}

	@NonNull
	public Date getDateUpdated() {
		return this.dateUpdated;
	}

	@Nullable
	public String getLocation() {
		return this.location;
	}

	@Nullable
	public String getDescription() {
		return this.description;
	}


	// -------------------- SET
	public void setId(int id) {
		this.id = id;
	}

	public Event setIdAssociation(@NonNull Integer idAssociation) {
		this.idAssociation = idAssociation;
		return this;
	}

	public Event setIdGoogleEvent(@NonNull String idGoogleEvent) {
		this.idGoogleEvent = idGoogleEvent;
		return this;
	}

	public Event setSummary(@NonNull String summary) {
		this.summary = summary;
		return this;
	}

	public Event setCanceled(@NonNull Boolean canceled) {
		this.canceled = canceled;
		return this;
	}

	public Event setDateStart(@NonNull Date dateStart) {
		this.dateStart = dateStart;
		return this;
	}

	public Event setDateEnd(@NonNull Date dateEnd) {
		this.dateEnd = dateEnd;
		return this;
	}

	public Event setDateUpdated(@NonNull Date dateUpdated) {
		this.dateUpdated = dateUpdated;
		return this;
	}

	public Event setLocation(@Nullable String location) {
		this.location = location;
		return this;
	}

	public Event setDescription(@Nullable String description) {
		this.description = description;
		return this;
	}


	// -------------------- OVERRIDE
	@SuppressLint("SimpleDateFormat")
	@NonNull
	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		return "Event {" +
				"id=" + this.id +
				", idAssociation='" + this.idAssociation + "'" +
				", idGoogleEvent=" + this.idGoogleEvent +
				", summary='" + this.summary + "'" +
				", canceled=" + this.canceled +
				", dateStart=" + dateFormat.format(this.dateStart) +
				", dateEnd=" + dateFormat.format(this.dateEnd) +
				", dateUpdated=" + dateFormat.format(this.dateUpdated) +
				", location='" + this.location + "'" +
				", description='" + this.description + "'" +
				'}';
	}
}
